// Stephen Kuhn
// 61135 Lab Exercise 2 - Playing Card
// 2/7/2022

#include <iostream>
#include <conio.h>

enum Suit
{
	SPADES = 0,
	DIAMONDS = 1,
	CLUBS = 2,
	HEARTS = 3
};

enum Rank
{
	TWO = 2,
	THREE = 3,
	FOUR = 4,
	FIVE = 5,
	SIX = 6,
	SEVEN = 7,
	EIGHT = 8,
	NINE = 9,
	TEN = 10,
	JACK = 11,
	QUEEN = 12,
	KING = 13,
	ACE = 14
};

struct Card
{
	Suit suit;		// Suit of Card
	Rank CardValue; // Rank Value of Playing Card

};

int main()
{

}